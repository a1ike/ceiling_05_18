$(document).ready(function () {

  $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  $('.c-members__cards').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    centerMode: false,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.c-portfolio__big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    fade: true,
    asNavFor: '.c-portfolio__small'
  });
  $('.c-portfolio__small').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.c-portfolio__big',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });


  $('.c-top__menu').on('click', function (e) {
    e.preventDefault();
    $('.c-header__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.a-advices__card-header').on('click', function (e) {
    e.preventDefault();
    $(this).children().next().toggleClass('a-advices__card-icon_closed');
    $(this).next().slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.a-rate__blur').on('click', function (e) {
    e.preventDefault();

    $('.a-steps__combo').slideToggle('fast', function (e) {
      // callback
    });
    $('.a-stepper__wrapper').slideToggle('fast', function (e) {
      // callback
    });

    $('html, body').animate({
      scrollTop: $('#steps').offset().top
    }, 500);
  });

  /* $('#main-form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var person = $('input[name="person"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var comment = $('input[name="comment"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        alert('Спасибо, ваша заявка отправлена!');
      }
      else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  }); */

});

